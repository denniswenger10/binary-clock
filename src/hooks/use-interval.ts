import { useEffect } from 'react'

const useInterval = (fn: () => void, intervalInMs: number) => {
  useEffect(() => {
    const interval = setInterval(() => {
      fn()
    }, intervalInMs)
    return () => clearInterval(interval)
  }, [fn, intervalInMs])
}

export { useInterval }
