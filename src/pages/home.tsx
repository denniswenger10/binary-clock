import React, { useState } from 'react'
import type { FC } from 'react'
import { Binary } from '@components/binary'
import { useInterval } from '@hooks'
import { dateToBinaryDate, dateToBinaryTime } from '@common/binary'
import { isiOS } from '@common'
import { Center, VStack } from '@chakra-ui/react'

const Home: FC = () => {
  const [binaryDate, setBinaryDate] = useState(
    dateToBinaryDate(new Date())
  )
  const [binaryTime, setBinaryTime] = useState(
    dateToBinaryTime(new Date())
  )
  const [_isiOS] = useState(isiOS())

  useInterval(() => {
    setBinaryDate(dateToBinaryDate(new Date()))
  }, 1000)

  useInterval(() => {
    setBinaryTime(dateToBinaryTime(new Date()))
  }, 100)

  return (
    <Center minH={_isiOS ? '-webkit-fill-available' : '100vh'}>
      <VStack spacing={10}>
        <Binary binaryData={binaryDate} defaultColor="red.600" />
        <Binary binaryData={binaryTime} />
      </VStack>
    </Center>
  )
}

export { Home }
