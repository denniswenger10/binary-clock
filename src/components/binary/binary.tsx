import React, { FC } from 'react'
import type { Bit as BitType } from '@common/binary'
import { HStack, VStack } from '@chakra-ui/layout'
import { chunkArray } from '@common'
import { Bit } from './bit'

interface BinaryProps {
  binaryData: BitType[][]
  colorMap?: Map<string, string>
  defaultColor?: string
}

const Binary: FC<BinaryProps> = ({
  binaryData,
  colorMap = new Map(),
  defaultColor = 'blue.500',
}) => {
  const chunks = chunkArray(2, binaryData)

  const getColor = (y: number, x: number) =>
    colorMap.get(`${y}${x}`) ?? defaultColor

  return (
    <HStack spacing={4}>
      {chunks.map((chunk: BitType[][], i) => (
        <HStack key={`chunk${i}`}>
          {chunk.map((bitArray, j) => (
            <VStack key={`column${j}`}>
              {bitArray.map((bit, l) => {
                return (
                  <Bit
                    key={`bit${j}${l}`}
                    bit={bit}
                    color={getColor(l, i * 2 + j)}
                  />
                )
              })}
            </VStack>
          ))}
        </HStack>
      ))}
    </HStack>
  )
}

export { Binary }
