import React, { FC } from 'react'
import type { Bit as BitType } from '@common/binary'
import { Box } from '@chakra-ui/react'

interface BitProps {
  bit: BitType
  color?: string
}

const Bit: FC<BitProps> = ({ bit, color = 'purple.500' }) => {
  return (
    <Box
      h="10"
      w="10"
      transition="all 300ms ease"
      bgColor={bit ? color : 'gray.700'}
      position="relative"
      boxShadow="0px 0px 13px 2px rgba(0,0,0,0.25)"
      borderRadius="5px"
      _after={{
        content: '""',
        position: 'absolute',
        transition: 'all 300ms ease-in',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        background: 'hsl(210, 100%, 90%)',
        willChange: 'opacity',
        opacity: bit ? 0.3 : 0,
        filter: bit ? 'blur(7px)' : '',
        zIndex: 1000,
      }}
    />
  )
}

export { Bit }
