import type { Bit } from './bit'

const numberToFourBitBinary = (
  number: number
): [Bit, Bit, Bit, Bit] => {
  const binary = (number >>> 0).toString(2)

  return (binary.length > 3 ? binary : `0000${binary}`)
    .slice(-4)
    .split('')
    .map((v) => +v) as [Bit, Bit, Bit, Bit]
}

export { numberToFourBitBinary }
