import { doubleDigit } from './double-digit'

const dateObjectToTimes = (
  date: Date
): [number, number, number, number, number, number] => {
  return [
    ...doubleDigit(date.getHours()),
    ...doubleDigit(date.getMinutes()),
    ...doubleDigit(date.getSeconds()),
  ]
}

const dateObjectToDate = (
  date: Date
): [number, number, number, number, number, number] => {
  return [
    ...doubleDigit(+`${date.getFullYear()}`.slice(-2)),
    ...doubleDigit(date.getMonth() + 1),
    ...doubleDigit(date.getDate()),
  ]
}

export { dateObjectToTimes, dateObjectToDate }
