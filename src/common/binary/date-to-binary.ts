import type { Bit } from './bit'
import {
  dateObjectToTimes,
  dateObjectToDate,
} from './date-object-conversion'
import { numberToFourBitBinary } from './number-to-four-bit-binary'

const dateToBinary =
  (
    dateConverter: (
      date: Date
    ) => [number, number, number, number, number, number]
  ) =>
  (date: Date) =>
    dateConverter(date).map(numberToFourBitBinary) as [
      [Bit, Bit, Bit, Bit],
      [Bit, Bit, Bit, Bit],
      [Bit, Bit, Bit, Bit],
      [Bit, Bit, Bit, Bit],
      [Bit, Bit, Bit, Bit],
      [Bit, Bit, Bit, Bit]
    ]

const dateToBinaryTime = dateToBinary(dateObjectToTimes)
const dateToBinaryDate = dateToBinary(dateObjectToDate)

export { dateToBinaryTime, dateToBinaryDate }
