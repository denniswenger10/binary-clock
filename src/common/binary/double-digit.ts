const doubleDigit = (digit: number): [number, number] =>
  digit < 10
    ? [0, digit]
    : (digit
        .toString()
        .split('')
        .map((i) => +i) as [number, number])

export { doubleDigit }
