const chunkArray = <T>(chunkSize = 1, list: T[] = []) => {
  if (chunkSize <= 0) throw 'Invalid chunk size'

  const newList = []

  for (var i = 0, len = list.length; i < len; i += chunkSize)
    newList.push(list.slice(i, i + chunkSize))

  return newList
}

export { chunkArray }
