type Test<ReturnValueType> =
  | boolean
  | ((param: ReturnValueType) => boolean)

/**
 * Will return the right value of the first function or boolean of the left that is true.
 * If none of them is true it will give the value of the second function.
 *
 * @example
 * ```ts
 * pickFirstTrue(
 *  [false, 'Apple'],
 *  [true, 'Banana']
 * )('Melon') // -> 'Banana'
 *
 * pickFirstTrue(
 *  [false, 'Apple'],
 *  [false, 'Banana']
 * )('Melon') // -> 'Melon'
 *
 * const isApple = (fruit: string) => fruit === 'Apple'
 * pickFirstTrue(
 *  [isApple, 'Apple'],
 *  [isApple, 'Banana']
 * )('Melon') // -> 'Apple'
 *
 *
 * ```
 */
const pickFirstTrue =
  <ReturnValueType>(
    ...testsAndValues: [Test<ReturnValueType>, ReturnValueType][]
  ) =>
  (valueIfAllFalse: ReturnValueType): ReturnValueType => {
    for (const [test, value] of testsAndValues) {
      if (typeof test == 'boolean' ? test : test(value)) {
        return value
      }
    }

    return valueIfAllFalse
  }

export { pickFirstTrue }
export type { Test }
