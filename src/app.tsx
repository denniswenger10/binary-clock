import React from 'react'
import type { FC } from 'react'
import { ChakraProvider } from '@chakra-ui/react'
import { Home } from '@pages'
import { defaultTheme } from './styles/theme'

const App: FC = () => {
  return (
    <ChakraProvider theme={defaultTheme}>
      <Home />
    </ChakraProvider>
  )
}

export { App }
