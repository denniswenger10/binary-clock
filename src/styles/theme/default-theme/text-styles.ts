import { css } from '@emotion/react'

const textStyles = {
  h1: {
    fontFamily: 'Nunito, sans-serif',
    fontStyle: 'normal',
    fontWeight: 700,
    fontSize: '32px',
    lineHeight: '43.6px',
    letterSpacing: '0em',
    textAlign: 'center',
  },
  h2: {
    fontFamily: 'Nunito, sans-serif',
    fontStyle: 'normal',
    fontWeight: 600,
    fontSize: '24px',
    lineHeight: '32.7px',
    letterSpacing: '0em',
    textAlign: 'center',
  },
  h3: {
    fontFamily: 'Nunito, sans-serif',
    fontStyle: 'normal',
    fontWeight: 600,
    fontSize: '16px',
    lineHeight: '21.8px',
    letterSpacing: '0em',
    textAlign: 'center',
  },
  p: {
    fontFamily: 'Nunito, sans-serif',
    fontStyle: 'normal',
    fontWeight: 300,
    fontSize: '14px',
    lineHeight: '19.1px',
    letterSpacing: '0em',
  },
  subHeader: {
    fontFamily: 'Nunito, sans-serif',
    fontStyle: 'normal',
    fontWeight: 300,
    fontSize: '16px',
    lineHeight: '21.8px',
    letterSpacing: '0em',
    color: 'gray.600',
  },
  button: {
    fontFamily: 'Nunito, sans-serif',
    fontStyle: 'normal',
    fontWeight: 700,
    fontSize: '16px',
    lineHeight: '21.8px',
    letterSpacing: '0em',
    textTransform: 'uppercase',
  },
  caption: {
    fontFamily: 'Nunito, sans-serif',
    fontStyle: 'italic',
    fontWeight: 300,
    fontSize: '14px',
    lineHeight: '19.1px',
    letterSpacing: '0px',
  },
  tableHead: {
    fontFamily: 'Nunito, sans-serif',
    fontStyle: 'normal',
    fontWeight: 700,
    fontSize: '12px',
    letterSpacing: '0.1em',
    textTransform: 'uppercase',
  },
}

const TEXT_STYLE = {
  code: {
    '1': css({
      fontFamily: '"Fira Code", monospace',
      fontStyle: 'normal',
      fontWeight: 400,
      fontSize: '16px',
      lineHeight: '24px',
      letterSpacing: '0em',
    }),
    '2': css({
      fontFamily: '"Fira Code", monospace',
      fontStyle: 'normal',
      fontWeight: 400,
      fontSize: '12px',
      lineHeight: '17px',
      letterSpacing: '0.02em',
    }),
  },
}

export { textStyles }
