import { extendTheme } from '@chakra-ui/react'
import { colors } from './colors'
import { global } from './global'
import { fonts } from './fonts'
import { textStyles } from './text-styles'

const defaultTheme = extendTheme({
  colors,
  fonts,
  textStyles,
  styles: {
    global,
  },
})

export { defaultTheme }
