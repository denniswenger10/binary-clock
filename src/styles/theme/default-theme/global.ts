const global = {
  html: {
    height: '100%',
  },
  body: {
    bg: 'gray.900',
    color: 'white',
    height: '100%',
  },
}

export { global }
