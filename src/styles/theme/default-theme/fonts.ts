const fonts = {
  body: 'Nunito, serif',
  heading: 'Nunito, serif',
}

export { fonts }
